# WordPress Skeleton

This is simply a skeleton repo for a WordPress site. Use it to jump-start our WordPress site repos. 

Run `npm install` and `gulp watch` to compile Sass files.