<?php
/**
* Scripts / Styles
*
* Handles front end scripts and styles.
*
* @package     CGD-Genesis-Child
* @since       1.0
* @author      Wes Cole <wes@cgd.io>
*/

function cgd_enqueue_scripts() {
	//wp_register_script( 'typekit', '//use.typekit.net/ovg1gdk.js');
	//wp_enqueue_script( 'typekit' );

	// Non jQuery Sitewide Stuff
	//wp_register_script( 'sitewide-nonjq', get_bloginfo('stylesheet_directory') . '/assets/js/sitewide-nonjq.js', array('typekit'));
	//wp_enqueue_script( 'sitewide-nonjq' );

	wp_register_script( 'sitewide', get_bloginfo('stylesheet_directory') . '/assets/js/sitewide.js', array('typekit','jquery') );
	wp_enqueue_script( 'sitewide' );
}

add_action( 'wp_enqueue_scripts', 'cgd_enqueue_scripts' );
