// Load plugins
var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var gulpif = require('gulp-if');
var sprite = require('css-sprite').stream;
var minify = require('gulp-minify-css');
var rename = require('gulp-rename');
var concat = require('gulp-concat');

// Sprite Generation
// generate sprite.png and _sprite.scss
gulp.task('sprites', function () {
  return gulp.src('assets/images/src/*.png')
    .pipe(sprite({
      name: 'sprite',
      style: '_sprite.scss',
      cssPath: 'assets/images/',
      processor: 'scss'
    }))
    .pipe(gulpif('*.png', gulp.dest('assets/images/'), gulp.dest('assets/styles/')));
});

// Styles
gulp.task('sass', function () {
    gulp.src('assets/styles/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./'))
    .pipe(notify({message: 'Sass task complete'}));
});

gulp.task('editor-sass', function() {
    gulp.src('assets/editor/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./'))
    .pipe(notify({message: 'Editor Sass task complete'}));
});


// Site Scripts
gulp.task('scripts', function() {
  return gulp.src(['assets/js/*.js', '!assets/js/*-min.js'])
  .pipe(jshint('.jshintrc'))
  .pipe(jshint.reporter('jshint-stylish'))
  .pipe(rename({ suffix: '-min' }))
  .pipe(gulp.dest('assets/js'))
  .pipe(notify({ message: 'Scripts task complete' }));
});

// Watch
gulp.task('watch', function() {
    gulp.watch('assets/images/src/*.png', ['sprites']);
    gulp.watch('assets/editor/*.scss', ['editor-sass']);
    gulp.watch('assets/styles/**/*.scss', ['sass']);
    gulp.watch('assets/js/!(*-min)*.js', ['scripts']);
});

// Default task
gulp.task('default', ['sprites', 'editor-sass', 'sass', 'scripts', 'watch']);
